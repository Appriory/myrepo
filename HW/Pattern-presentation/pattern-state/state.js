        var tmp = 0;

    class OrderStatus {
        constructor(name, nextStatus) {
            this.name = name;
            this.nextStatus = nextStatus;
    }
        next() {
            return new this.nextStatus();
        }
    }

    class WaitingForPayment extends OrderStatus {
        constructor() {
            super('waitingForPayment', Shipping);
        }
    }

    class Shipping extends OrderStatus {
        constructor() {
            super('shipping', Delivered);
        }
    }

    class Delivered extends OrderStatus {
        constructor() {
            super('delivered', Delivered);
        }
    }

    class Order {
        constructor() {
            this.state = new WaitingForPayment();
        }

        nextState() {
            this.state = this.state.next();
        };

        cancelOrder(mood) {

            if(this.state.name === 'waitingForPayment' && mood === 'bad') {
                console.log('Order was canceled because of bad mood!!!');
                tmp++;
            } else {
                return;
            }
        }
    }
    let myOrder = new Order();

	let timer1 = setTimeout(function(cancelOrder) {
		console.log(myOrder.state.name);
		myOrder.cancelOrder('bad'); //You can remove this.'bad' from scopes and behavior of program will change.
			if(tmp === 1) {
				return;
			} else {
				let timer2 = setTimeout(function() {
					myOrder.nextState();
					console.log(myOrder.state.name);

					let timer2 = setTimeout(function() {
						myOrder.nextState();
						console.log(myOrder.state.name);
					}, 2500);
				}, 2500);
			};
	}, 2500);