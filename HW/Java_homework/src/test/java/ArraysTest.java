import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ArraysTest {

    @Test
    public void findMinElement() {
        Arrays arrays = new Arrays();
        int actual = Arrays.findMinElement(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void findMinIndex() {
        Arrays arrays = new Arrays();
        int actual = Arrays.findMinIndex(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void findMaxElement() {
        Arrays arrays = new Arrays();
        int actual = Arrays.findMaxElement(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 800;
        assertEquals(expected,actual);
    }

    @Test
    public void findMaxIndex() {
        Arrays arrays = new Arrays();
        int actual = Arrays.findMaxIndex(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 1;
        assertEquals(expected,actual);
    }

    @Test
    public void sumOddIndexElement() {
        Arrays arrays = new Arrays();
        int actual = Arrays.sumOddIndexElement(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 845;
        assertEquals(expected,actual);
    }

    @Test
    public void arRevers() {
        Arrays arrays = new Arrays();
        List<Integer> actual = Arrays.arRevers(new int [] {500, 800, 500, 44, 55, 1});
        int[] expected = new int[]{1, 55, 44, 500, 800, 500};
    }
    private void assertArrayEquals(int[] expected, List<Integer> actual) {
    }



    @Test
    public void oddQuant() {
        Arrays arrays = new Arrays();
        int actual = Arrays.oddQuant(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 2;
        assertEquals(expected,actual);
    }

}