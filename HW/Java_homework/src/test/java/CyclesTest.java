import org.junit.Test;

import static org.junit.Assert.*;

public class CyclesTest {

    @Test
    public void sumAndQuantity() {
        Cycles cycles = new Cycles();
        String actual = Cycles.sumAndQuantity();
        String expected = "sum = 2450 quan = 50";
        assertEquals(expected, actual);
    }

    @Test
    public void CheckSimpleNum() {
        Cycles cycles = new Cycles();
        String actual = Cycles.CheckSimpleNum(8);
        String expected = "Non prime number";
        assertEquals(expected, actual);
    }

    @Test
    public void rootOfNum() {
        Cycles cycles = new Cycles();
        int actual = Cycles.rootOfNum(860);
        int expected = 29;
        assertEquals(expected, actual);
    }

    @Test
    public void fucOfNum() {
        Cycles cycles = new Cycles();
        int actual = Cycles.fucOfNum(9);
        int expected = 362880;
        assertEquals(expected, actual);
    }

    @Test
    public void sumElementsOfNumber() {
        Cycles cycles = new Cycles();
        int actual = Cycles.sumElementsOfNumber(234);
        int expected = 9;
        assertEquals(expected, actual);
    }

    @Test
    public void mirrorNum() {
        Cycles cycles = new Cycles();
        int actual = Cycles.mirrorNum(234);
        int expected = 432;
        assertEquals(expected, actual);
    }


}