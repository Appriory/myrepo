import org.junit.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ArrHwTest {

    @Test
    public void findMinElement() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.findMinElement(new int[]{500, 800, 500, 44, 55, 1});
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void findMinIndex() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.findMinIndex(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void findMaxElement() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.findMaxElement(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 800;
        assertEquals(expected,actual);
    }

    @Test
    public void findMaxIndex() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.findMaxIndex(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 1;
        assertEquals(expected,actual);
    }

    @Test
    public void sumOddIndexElement() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.sumOddIndexElement(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 845;
        assertEquals(expected,actual);
    }

    @Test
    public void arRevers() {
        ArrHw arrhw = new ArrHw();
        List<Integer> actual = ArrHw.arRevers(new int [] {500, 800, 500, 44, 55, 1});
        int[] expected = new int[]{1, 55, 44, 500, 800, 500};
    }
    private void assertArrayEquals(int[] expected, List<Integer> actual) {
    }



    @Test
    public void oddQuant() {
        ArrHw arrhw = new ArrHw();
        int actual = ArrHw.oddQuant(new int [] {500, 800, 500, 44, 55, 1});
        int expected = 2;
        assertEquals(expected,actual);
    }

    @Test
    public void selectSort() {
        ArrHw arrhw = new ArrHw();
        String actual = ArrHw.selectSort(new int [] {500, 800, 500, 44, 55, 1});
        String expected = Arrays.toString(new int[]{1, 44, 55, 500, 500, 800});
        assertEquals(expected,actual);
    }

    @Test
    public void insertSort() {
        ArrHw arrhw = new ArrHw();
        String actual = ArrHw.insertSort(new int [] {500, 800, 500, 44, 55, 1});
        String expected = Arrays.toString(new int[]{1, 44, 55, 500, 500, 800});
        assertEquals(expected,actual);
    }

    @Test
    public void bubbleSort() {
        ArrHw arrhw = new ArrHw();
        String actual = ArrHw.bubbleSort(new int [] {500, 800, 500, 44, 55, 1});
        String expected = Arrays.toString(new int[]{1, 44, 55, 500, 500, 800});
        assertEquals(expected,actual);
    }

}
