import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionsTest {

    @Test
    public void getDayName() {
        Functions functions = new Functions();
        String actual = Functions.getDayName(5);
        String expected = "Friday";
        assertEquals(expected, actual);
    }

    @Test
    public void deckardDistBetweenDots() {
        Functions functions = new Functions();
        int actual = Functions.deckardDistBetweenDots(1, 2, 3, 4);
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    public void makeStringFromThousandNum() {
        Functions functions = new Functions();
        String actual = Functions.makeStringFromThousandNum(408);
        String expected = "four hundred eight";
        assertEquals(expected, actual);
    }


}