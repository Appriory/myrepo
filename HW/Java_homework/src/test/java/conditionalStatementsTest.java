import org.junit.Test;

import static org.junit.Assert.*;

public class conditionalStatementsTest {

    @Test
    public void oddEvenFunction() {
        conditionalStatements conditionalstatements = new conditionalStatements();
        int actual = conditionalStatements.oddEvenFunction(2, 3);
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    public void studBallProgramm() {
        conditionalStatements conditionalstatements = new conditionalStatements();
        String actual = conditionalStatements.studBallProgramm(56);
        String expected = "D";
        assertEquals(expected, actual);
    }

    @Test
    public void sumPosNumbers() {
        conditionalStatements conditionalstatements = new conditionalStatements();
        int actual = conditionalStatements.sumPosNumbers(2, 3, -4);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void sumOrMultiply() {
        conditionalStatements conditionalstatements = new conditionalStatements();
        int actual = conditionalStatements.sumOrMultiply(1, 1, 1);
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    public void numOfQuart() {
        conditionalStatements conditionalstatements = new conditionalStatements();
        String actual = conditionalStatements.numOfQuart(0, 0);
        String expected = "A point is on an intersection of coordinates";
        assertEquals(expected, actual);
    }

}