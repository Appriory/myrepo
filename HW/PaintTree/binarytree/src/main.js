
const BinaryTree = function(){
    this.root = null;
};

const Node = function(value){
    this.value = value;
    this.right = null;
    this.left = null;
};

BinaryTree.prototype.init = function(initialArray){
    this.root = null;
    for(let i = 0; i < initialArray.length; i++){
        this.add(initialArray[i]);
    }
};

BinaryTree.prototype.add = function(value){
    this.root = this.addNode(this.root, value);
};

BinaryTree.prototype.addNode = function(node, value){
    if(node === null){
        node = new Node(value);
    }else if(value > node.value){
        node.right = this.addNode(node.right, value);
    }else if(value < node.value){
        node.left = this.addNode(node.left, value);
    }else if(value === node.value){
        return node;
    }
    return node;
};

BinaryTree.prototype.toArray = function(){
    let arr = [];
    return this.toArrayNode(this.root, arr);
}

BinaryTree.prototype.toArrayNode = function(node, arr) {
    if(node === null){
        return;
    }
    if(node.left === null && node.right === null){
        arr.push(node.value);
        return;
    }
    this.toArrayNode(node.left, arr);
    arr.push(node.value);
    this.toArrayNode(node.right, arr);
    return arr;
};

BinaryTree.prototype.getLeaves = function(){
    return this.getLeavesNode(this.root);
};

BinaryTree.prototype.getLeavesNode = function(node){
    if(node.left === null && node.right === null){
        return 1;
    }else if(node.left === null) {
        return this.getLeavesNode(node.right);
    }else if(node.right === null){
        return this.getLeavesNode(node.left);
    }
    return this.getLeavesNode(node.left) + this.getLeavesNode(node.right);
};

BinaryTree.prototype.getNodes = function(){
    return this.getNodesNode(this.root);
};

BinaryTree.prototype.getNodesNode = function(node){
    if(node.left === null && node.right === null) {
        return 1;
    }else if(node.left === null){
        return 1 + this.getNodesNode(node.right);
    }else if(node.right === null){
        return 1 + this.getNodesNode(node.left);
    }
    return 1 + this.getNodesNode(node.right) + this.getNodesNode(node.left);
};

BinaryTree.prototype.getDeep = function () {
    return this.getDeepNode(this.root);
};

BinaryTree.prototype.getDeepNode = function (node) {
    if (node.left === null && node.right === null) {
        return 1;
    } else if (node.left === null) {
        return 1 + this.getDeepNode(node.right);
    }else if(node.right === null) {
        return 1 + this.getDeepNode(node.left);
    }
    return 1 + Math.max(this.getDeepNode(node.left), this.getDeepNode(node.right));
};

BinaryTree.prototype.getWight = function () {
    let max = 0;
    let res = 0;
    for (let i = 1; i < fuckingBinary.getDeep(); i++) {
        max = Math.max(this.getWightNode(this.root, fuckingBinary.getDeep()), this.getWightNode(this.root, fuckingBinary.getDeep() - i));
        res = Math.max(res,max);
    }
    return res;
};

BinaryTree.prototype.getWightNode = function (node, level) {
    if (node === null) return 0;
    if (level === 1) return 1;
    return this.getWightNode(node.left, level - 1) + this.getWightNode(node.right, level - 1);
};

BinaryTree.prototype.clear = function () {
    this.root = null;
};

BinaryTree.prototype.find = function(value)  {
    return this.findNode(this.root,value);
}; // =>>Node

BinaryTree.prototype.findNode = function (node, value) {
    if (node === null) return;
    if (node.value === value) {
        return node;
    }
    return this.findNode(node.left, value) || this.findNode(node.right, value);
};

const fuckingBinary = new BinaryTree();
var arr=[100,80,120,60,90,140,110,150,170,105,180,190];
fuckingBinary.init(arr);
console.log(fuckingBinary);

var draw = false;
var clientArr = [];
var paint = document.getElementById('canvas');
var build = document.getElementById("build");
var cleaner=document.getElementById("clean");
var ctx = paint.getContext("2d");
var circleSize=10;
var startPoint=(paint.width/2)-(circleSize/2);
var step= (fuckingBinary.getDeep())*50;
cleaner.addEventListener("click", ()=>{    ctx.clearRect(0, 0, paint.width, paint.height);});
console.log(fuckingBinary.getDeep());
BinaryTree.prototype.draw = function()  {
    console.log(startPoint);
    this.drawTree(this.root,  startPoint+step, 20-step, true, step );
}; // =>>Node

BinaryTree.prototype.drawTree = function (node, x, y, position, step) {
    if(step<15){
        step=15;
    }
    if (node === null){
        if(position){
            drawTree("null", x-=step, y+=step)
        }else {drawTree("null", x+=step, y+=step);}
        return;}

    // if(node.right===null && node.left===null){
    //     //     if(position){
    //     //         drawTree(node.value, x-=15, y+=15)
    //     //     }else {drawTree(node.value, x+=15, y+=15);}
    //     // } else
    if(position){
        drawTree(node.value, x-=step, y+=step);
    }else {drawTree(node.value, x+=step, y+=step)}

    return this.drawTree(node.left, x, y, true, step/=1.8) || this.drawTree(node.right, x, y,false, step);
};
fuckingBinary.draw();
console.log(fuckingBinary.getWight());

function drawTree(value, x, y) {
    ctx.beginPath();
    ctx.textAlign = "center";
    ctx.textBaseline="middle";
    ctx.fillText(value, x, y);
    ctx.arc(x, y, circleSize, 0, 2 * Math.PI, false);
    ctx.stroke();
}
