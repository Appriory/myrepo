const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");



module.exports = {

    entry:"./src/index.js",
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "index.[hash:8].js"
    },

    module: {
        rules: [
                {
                    test: /\.less|css&/,
                    exclude: /node_modules/,
                    use: [
                            MiniCssExtractPlugin.loader,
                            'css-loader',
                            'less-loader',
                    ],
                },
                       {
                        test: /\.(png|svg|jpg|gif)$/,
                        use: [
                           'file-loader',
                        ],
                       },
        ]
    },



    plugins: [
        new HtmlWebpackPlugin ({
            filename: "index.[hash:8].html",
            template: "./public/index.html"
        }),
        new CleanWebpackPlugin (),
        new MiniCssExtractPlugin ({
            filename: "index.[hash:8].css",
        })
    ]
}