/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _styles_index_less__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles/index.less */ \"./src/styles/index.less\");\n/* harmony import */ var _styles_index_less__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_index_less__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\n    let modal = document.getElementById(\"myModal\");\r\n    let openModal = document.getElementById(\"settingsBtn\");\r\n    let closeModal = document.getElementById(\"modalClose\");\r\n    let convert = document.getElementById(\"convert\");\r\n    let forArabPeople = document.querySelector(\".wrapper\");\r\n    let selectChange = document.getElementById(\"locations\");\r\n\r\n    (function(){\r\n        let select = document.querySelector('#locations');\r\n        if (localStorage.selectedIndex !== undefined) {\r\n            select.selectedIndex = localStorage.selectedIndex;\r\n        }\r\n        select.onchange = function() {\r\n            localStorage.selectedIndex = this.selectedIndex;\r\n        }\r\n           changeTextBySelectChoice();\r\n    })()\r\n    let objectsArray = [\r\n        {name: \"meter\", meter: 1, yard: 1.094, foot: 3.281, mile: 0.000621 ,verst: 0.000937,},\r\n        {name: \"yard\", yard: 1, meter: 0.914, foot: 3, mile: 0.000568, verst: 0.000856,},\r\n        {name: \"foot\", foot: 1, meter: 0.304, yard: 0.333, mile: 0.000189, verst: 0.000285,},\r\n        {name: \"mile\", mile: 1, meter: 1609, yard: 1760, foot: 5280, verst: 0.662,},\r\n        {name: \"verst\", verst: 1, meter: 1067, yard: 1167, foot: 3500, mile: 0.663,}\r\n    ];\r\n   function getConvert() {\r\n         let valueForConvert = document.getElementById(\"valueFor\");\r\n         let valueOnConvert = document.getElementById(\"valueOn\");\r\n         let outputResult = document.getElementById(\"output\");\r\n         let numbToConvert = document.getElementById(\"inputNum\");\r\n         let result;\r\n\r\n         numbToConvert = Number(numbToConvert.value);\r\n\r\n         valueOnConvert = valueOnConvert.value;\r\n         valueForConvert = valueForConvert.value;\r\n         console.log(valueOnConvert, valueForConvert);\r\n\r\n         let objValue = objectsArray.find(obj => obj.name === valueForConvert);\r\n         console.log(objValue);\r\n         result = (objValue[valueOnConvert]);\r\n         outputResult.value = numbToConvert * result;\r\n\r\nif (numbToConvert == 0) {\r\noutputResult.value = \"\";\r\n}\r\n\r\n    }\r\n\r\nfunction changeText() {\r\n    let x = document.getElementById(\"locations\");\r\n    let elementsList = document.getElementsByClassName(\"textForChange\");\r\nconsole.log(elementsList);\r\n    x = x.value;\r\n    if(x === \"Rus\") {\r\n       convert.value = \"Вычислить\";\r\n                       rtlDirectionRemove();\r\n               for (let i = 0; i < elementsList.length; i++) {\r\n                  if(elementsList[i].value === \"meter\") {\r\n                       elementsList[i].text = \"Метры\";\r\n                  } else if(elementsList[i].value === \"yard\") {\r\n                       elementsList[i].text = \"Ярды\";\r\n                  } else if(elementsList[i].value === \"mile\") {\r\n                       elementsList[i].text = \"Мили\";\r\n                  } else if(elementsList[i].value === \"foot\") {\r\n                       elementsList[i].text = \"Футы\";\r\n                  } else if(elementsList[i].value === \"verst\") {\r\n                       elementsList[i].text = \"Версты\";\r\n                  } else if(elementsList[i].placeholder === \"Put your value\" || elementsList[i].placeholder === \"أدخل القيمة\") {\r\n                       elementsList[i].placeholder = \"Введите значение\";\r\n                  } else if(elementsList[i].placeholder === \"Result\" || elementsList[i].placeholder === \"يؤدي\") {\r\n                       elementsList[i].placeholder = \"Результат\";\r\n                  }\r\n               }\r\n    }\r\n    if(x == \"Eng\") {\r\n                convert.value = \"calculate\";\r\n                rtlDirectionRemove();\r\n        for (let i = 0; i < elementsList.length; i++) {\r\n           if(elementsList[i].value === \"meter\") {\r\n                elementsList[i].text = \"Meters\";\r\n           } else if(elementsList[i].value === \"yard\") {\r\n                elementsList[i].text = \"Yards\";\r\n           } else if(elementsList[i].value === \"mile\") {\r\n                elementsList[i].text = \"Miles\";\r\n           } else if(elementsList[i].value === \"foot\") {\r\n                elementsList[i].text = \"Foots\";\r\n           } else if(elementsList[i].value === \"verst\") {\r\n                elementsList[i].text = \"Versts\";\r\n           } else if(elementsList[i].placeholder === \"Введите значение\" || elementsList[i].placeholder === \"أدخل القيمة\") {\r\n                elementsList[i].placeholder = \"Put your value\";\r\n           } else if(elementsList[i].placeholder === \"Результат\" || elementsList[i].placeholder === \"يؤدي\") {\r\n                elementsList[i].placeholder = \"Result\";\r\n           }\r\n        }\r\n    }\r\n    if(x == \"Arb\") {\r\n                convert.value = \"حساب\";\r\n                rtlDirection();\r\n        for (let i = 0; i < elementsList.length; i++) {\r\n           if(elementsList[i].value === \"meter\") {\r\n                elementsList[i].text = \"متر\";\r\n           } else if(elementsList[i].value === \"yard\") {\r\n                elementsList[i].text = \"ياردة\";\r\n           } else if(elementsList[i].value === \"mile\") {\r\n                elementsList[i].text = \"ميل\";\r\n           } else if(elementsList[i].value === \"foot\") {\r\n                elementsList[i].text = \"قدم\";\r\n           } else if(elementsList[i].value === \"verst\") {\r\n                elementsList[i].text = \"معالم\";\r\n           } else if(elementsList[i].placeholder === \"Введите значение\" || elementsList[i].placeholder === \"Put your value\") {\r\n                elementsList[i].placeholder = \"أدخل القيمة\";\r\n           } else if(elementsList[i].placeholder === \"Результат\" || elementsList[i].placeholder === \"Result\") {\r\n                elementsList[i].placeholder = \"يؤدي\";\r\n           }\r\n        }\r\n    }\r\n}\r\n\r\nfunction changeTextBySelectChoice() {\r\n        changeText();\r\n}\r\n\r\n    function closeModalWindow() {\r\n          modal.style.display = \"none\";\r\n //         changeText();\r\n    }\r\n    function openModalWindow() {\r\n         modal.style.display = \"block\";\r\n    }\r\n    function rtlDirection() {\r\n        forArabPeople.className += \" arab_language\";\r\n    }\r\n  function rtlDirectionRemove() {\r\n        forArabPeople.className -= \" arab_language\";\r\n    }\r\n     closeModal.addEventListener(\"click\", closeModalWindow);\r\n     openModal.addEventListener(\"click\", openModalWindow);\r\n     convert.addEventListener(\"click\", getConvert);\r\n     selectChange.addEventListener(\"change\", changeTextBySelectChoice);\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/styles/index.less":
/*!*******************************!*\
  !*** ./src/styles/index.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/styles/index.less?");

/***/ })

/******/ });