import "./styles/index.less";

    let modal = document.getElementById("myModal");
    let openModal = document.getElementById("settingsBtn");
    let closeModal = document.getElementById("modalClose");
    let convert = document.getElementById("convert");
    let forArabPeople = document.querySelector(".wrapper");
    let selectChange = document.getElementById("locations");

    (function(){
        let select = document.querySelector('#locations');
        if (localStorage.selectedIndex !== undefined) {
            select.selectedIndex = localStorage.selectedIndex;
        }
        select.onchange = function() {
            localStorage.selectedIndex = this.selectedIndex;
        }
           changeTextBySelectChoice();
    })()
    let objectsArray = [
        {name: "meter", meter: 1, yard: 1.094, foot: 3.281, mile: 0.000621 ,verst: 0.000937,},
        {name: "yard", yard: 1, meter: 0.914, foot: 3, mile: 0.000568, verst: 0.000856,},
        {name: "foot", foot: 1, meter: 0.304, yard: 0.333, mile: 0.000189, verst: 0.000285,},
        {name: "mile", mile: 1, meter: 1609, yard: 1760, foot: 5280, verst: 0.662,},
        {name: "verst", verst: 1, meter: 1067, yard: 1167, foot: 3500, mile: 0.663,}
    ];
   function getConvert() {
         let valueForConvert = document.getElementById("valueFor");
         let valueOnConvert = document.getElementById("valueOn");
         let outputResult = document.getElementById("output");
         let numbToConvert = document.getElementById("inputNum");
         let result;

         numbToConvert = Number(numbToConvert.value);

         valueOnConvert = valueOnConvert.value;
         valueForConvert = valueForConvert.value;
         console.log(valueOnConvert, valueForConvert);

         let objValue = objectsArray.find(obj => obj.name === valueForConvert);
         console.log(objValue);
         result = (objValue[valueOnConvert]);
         outputResult.value = numbToConvert * result;

if (numbToConvert == 0) {
outputResult.value = "";
}

    }

function changeText() {
    let x = document.getElementById("locations");
    let elementsList = document.getElementsByClassName("textForChange");
console.log(elementsList);
    x = x.value;
    if(x === "Rus") {
       convert.value = "Вычислить";
                       rtlDirectionRemove();
               for (let i = 0; i < elementsList.length; i++) {
                  if(elementsList[i].value === "meter") {
                       elementsList[i].text = "Метры";
                  } else if(elementsList[i].value === "yard") {
                       elementsList[i].text = "Ярды";
                  } else if(elementsList[i].value === "mile") {
                       elementsList[i].text = "Мили";
                  } else if(elementsList[i].value === "foot") {
                       elementsList[i].text = "Футы";
                  } else if(elementsList[i].value === "verst") {
                       elementsList[i].text = "Версты";
                  } else if(elementsList[i].placeholder === "Put your value" || elementsList[i].placeholder === "أدخل القيمة") {
                       elementsList[i].placeholder = "Введите значение";
                  } else if(elementsList[i].placeholder === "Result" || elementsList[i].placeholder === "يؤدي") {
                       elementsList[i].placeholder = "Результат";
                  }
               }
    }
    if(x == "Eng") {
                convert.value = "calculate";
                rtlDirectionRemove();
        for (let i = 0; i < elementsList.length; i++) {
           if(elementsList[i].value === "meter") {
                elementsList[i].text = "Meters";
           } else if(elementsList[i].value === "yard") {
                elementsList[i].text = "Yards";
           } else if(elementsList[i].value === "mile") {
                elementsList[i].text = "Miles";
           } else if(elementsList[i].value === "foot") {
                elementsList[i].text = "Foots";
           } else if(elementsList[i].value === "verst") {
                elementsList[i].text = "Versts";
           } else if(elementsList[i].placeholder === "Введите значение" || elementsList[i].placeholder === "أدخل القيمة") {
                elementsList[i].placeholder = "Put your value";
           } else if(elementsList[i].placeholder === "Результат" || elementsList[i].placeholder === "يؤدي") {
                elementsList[i].placeholder = "Result";
           }
        }
    }
    if(x == "Arb") {
                convert.value = "حساب";
                rtlDirection();
        for (let i = 0; i < elementsList.length; i++) {
           if(elementsList[i].value === "meter") {
                elementsList[i].text = "متر";
           } else if(elementsList[i].value === "yard") {
                elementsList[i].text = "ياردة";
           } else if(elementsList[i].value === "mile") {
                elementsList[i].text = "ميل";
           } else if(elementsList[i].value === "foot") {
                elementsList[i].text = "قدم";
           } else if(elementsList[i].value === "verst") {
                elementsList[i].text = "معالم";
           } else if(elementsList[i].placeholder === "Введите значение" || elementsList[i].placeholder === "Put your value") {
                elementsList[i].placeholder = "أدخل القيمة";
           } else if(elementsList[i].placeholder === "Результат" || elementsList[i].placeholder === "Result") {
                elementsList[i].placeholder = "يؤدي";
           }
        }
    }
}

function changeTextBySelectChoice() {
        changeText();
}

    function closeModalWindow() {
          modal.style.display = "none";
 //         changeText();
    }
    function openModalWindow() {
         modal.style.display = "block";
    }
    function rtlDirection() {
        forArabPeople.className += " arab_language";
    }
  function rtlDirectionRemove() {
        forArabPeople.className -= " arab_language";
    }
     closeModal.addEventListener("click", closeModalWindow);
     openModal.addEventListener("click", openModalWindow);
     convert.addEventListener("click", getConvert);
     selectChange.addEventListener("change", changeTextBySelectChoice);