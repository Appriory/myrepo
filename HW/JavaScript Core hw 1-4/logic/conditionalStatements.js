
	function oddEvenFunction(a, b) {
	  if(a % 2 === 0) {
		return a * b;
		}
		return a + b;
	}

	function studBallProgram(x) {
	  if(x <= 19) {
		return 'F';
	    } else if(x <= 39) {
		return 'E';
		} else if(x <= 59) {
		return 'D';
		} else if(x <= 74) {
		return 'C';
		} else if(x <= 89) {
		return 'B';
		} else {
		 return 'A';
		}
    }


	function sumPosNumbers(a,b,c) {
	  var tmp = 0;
	  if(a > 0)
	    tmp += a;
	  if(b > 0)
		tmp += b;
	  if(c > 0)
		tmp += c;
	  return tmp;
	}

	function plusOrMultiply(a, b, c) {
	  var product = a * b * c;
	  var sum = a + b + c;
	  return product > sum ? product + 3 : sum + 3;
	}

	function numOfQuart(x,y) {
	  if(x > 0 && y > 0) {
	  return "A point is in first quart"
	  } else if(x < 0 && y > 0) {
	  return "A point is in second quart"
	  } else if(x < 0 && y < 0) {
	  return "A point is in third quart"
	  } else if(x > 0 && y < 0) {
	  return "A point is in four quart"
	  } else if(x === 0 && y === 0) {
      return "A point is on the intersection of coordinates"
      } else if(x > 0 && y === 0) {
      return "A point is between first and four quart on a x axis"
      } else if(x === 0 && y > 0) {
      return "A point is between first and second quart on a y axis"
      }
	}

