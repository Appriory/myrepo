

    describe("sumAndQuantity", function () {
        it("Returns the sum and number of positive elements of an array. A result should be 5.30", function () {
            assert.equal(sumAndQuantity([1,2,3,4,5,6,7,8,9,10]), 5.30);
        });

        it("Returns the sum and number of positive elements of an array. A result should be 6.42", function () {
            assert.equal(sumAndQuantity([1,2,3,4,5,6,7,8,9,10,11,12]), 6.42);
        });

    });

    describe("CheckSimpleNum", function () {
            it("Defines a prime and non prime number. A result should be 'Prime number'", function () {
                assert.equal(CheckSimpleNum(11), "Prime number");
            });

            it("Defines a prime and non prime number. A result should be 'Non prime number'", function () {
                assert.equal(CheckSimpleNum(12), "Non prime number");
            });

            it("Defines a prime and non prime number. A result should be 'Prime number'", function () {
                assert.equal(CheckSimpleNum(97), "Prime number");
            });
        });

        describe("rootOfNum", function () {
            it("Defines a root of number. A result should be 7", function () {
                assert.equal(rootOfNum(56), 7);
            });

            it("Defines a prime and non prime number. A result should be 8", function () {
                assert.equal(rootOfNum(77), 8);
            });

            it("Defines a prime and non prime number. A result should be 11", function () {
                assert.equal(rootOfNum(125), 11);
            });
        });

         describe("facOfNum", function () {
            it("Defines factorial of number. A result should be 362880", function () {
                assert.equal(facOfNum(9), 362880);
            });

            it("Defines factorial of number. A result should be 5040", function () {
                assert.equal(facOfNum(7), 5040);
            });

            it("Defines factorial of number. A result should be 120", function () {
                assert.equal(facOfNum(5), 120);
            });
        });

         describe("sumUserNum", function () {
            it("Create a sum of user numbers. A result should be 14", function () {
                assert.equal(sumUserNum(20345), 14);
            });

            it("Create a sum of user numbers. A result should be 12", function () {
                assert.equal(sumUserNum(1281), 12);
            });

            it("Create a sum of user numbers. A result should be 6", function () {
                assert.equal(sumUserNum(123), 6);
            });
        });

         describe("mirrorNum", function () {
            it("Reflect user number. A result should be 102", function () {
                assert.equal(mirrorNum(201), 102);
            });

            it("Reflect user number. A result should be 321", function () {
                assert.equal(mirrorNum(123), 321);
            });

            it("Reflect user number. A result should be 654", function () {
                assert.equal(mirrorNum(456), 654);
            });
        });