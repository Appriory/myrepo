

         describe("FindMinElement", function () {
            it("Seeking minimal element in array. A result should be 16", function () {
                assert.equal(FindMinElement([35,46,32,16,58]), 16);
            });

            it("Seeking minimal element in array. A result should be 87", function () {
                assert.equal(FindMinElement([255,321,233,87,44]), 44);
            });

            it("Seeking minimal element in array. A result should be 1050", function () {
                assert.equal(FindMinElement([1050,4567,3333,2222,1546]), 1050);
            });
        });

        describe("FindMinIndex", function () {
             it("Seeking minimal index in array. A result should be 3", function () {
                 assert.equal(FindMinIndex([35,46,32,16,58]), 3);
             });

             it("Seeking minimal index in array. A result should be 4", function () {
                 assert.equal(FindMinIndex([255,321,233,87,44]), 4);
             });

             it("Seeking minimal index in array. A result should be 0", function () {
                 assert.equal(FindMinIndex([1050,4567,3333,2222,1546]), 0);
             });
         });


         describe("FindMaxElement", function () {
             it("Seeking maximal element in array. A result should be 58", function () {
                 assert.equal(FindMaxElement([35,46,32,16,58]), 58);
             });

             it("Seeking maximal element in array. A result should be 321", function () {
                 assert.equal(FindMaxElement([255,321,233,87,44]), 321);
             });

             it("Seeking maximal element in array. A result should be 4567", function () {
                 assert.equal(FindMaxElement([1050,4567,3333,2222,1546]), 4567);
             });
         });


          describe("FindMaxIndex", function () {
             it("Seeking maximal index in array. A result should be 4", function () {
                 assert.equal(FindMaxIndex([35,46,32,16,58]), 4);
             });

             it("Seeking maximal index in array. A result should be 1", function () {
                 assert.equal(FindMaxIndex([255,321,233,87,44]), 1);
             });

             it("Seeking maximal index in array. A result should be 1", function () {
                 assert.equal(FindMaxIndex([1050,4567,3333,2222,1546]), 1);
             });
         });

          describe("sumOddIndexElement", function () {
             it("Counts sum of elements with non-positive index. A result should be 62", function () {
                 assert.equal(sumOddIndexElement([35,46,32,16,58]), 62);
             });

             it("Counts sum of elements with non-positive index. A result should be 70", function () {
                 assert.equal(sumOddIndexElement([35,46,32,16,58,8]), 70);
             });

             it("Counts sum of elements with non-positive index. A result should be 90", function () {
                 assert.equal(sumOddIndexElement([35,46,32,16,58,8,72,20]), 90);
             });
         });

         describe("arRevers", function () {
             it("Does revers of array. A result should be [58,16,32,46,35]", function () {
                assert.deepEqual(arRevers([135,456,320,160,508]), [508,160,320,456,135]);
             });

             it("Does revers of array. A result should be [8,58,16,32,46,35]", function () {
                 assert.deepEqual(arRevers([35,46,32,16,58,8]), [8,58,16,32,46,35]);
             });

             it("Does revers of array. A result should be [20,72,8,58,16,32,46,35]", function () {
                 assert.deepEqual(arRevers([35,46,32,16,58,8,72,20]), [20,72,8,58,16,32,46,35]);
             });
         });

         describe("oddQuant", function () {
             it("Counts quantity non-positive elements in array. A result should be 1", function () {
                 assert.equal(oddQuant([135,456,320,160,508]), 1);
             });

             it("Counts quantity non-positive elements in array. A result should be 3", function () {
                 assert.equal(oddQuant([35,46,32,17,58,7]), 3);
             });

             it("Counts quantity non-positive elements in array. A result should be 8", function () {
                 assert.equal(oddQuant([31,41,31,11,51,1,71,21]), 8);
             });
         });

          describe("ChangeSides", function () {
             it("Changes sides of array. A result should be [160,508,604,320,456,135]", function () {
                assert.deepEqual(ChangeSides([135,456,320,160,508,604]), [160,508,604,135,456,320]);
             });

             it("Changes sides of array. A result should be [16,58,8,35,46,32]", function () {
                 assert.deepEqual(ChangeSides([35,46,32,16,58,8]), [16,58,8,35,46,32]);
             });

             it("Changes sides of array. A result should be [58,8,72,20,35,46,32,16]", function () {
                 assert.deepEqual(ChangeSides([35,46,32,16,58,8,72,20]), [58,8,72,20,35,46,32,16]);
             });
         });

         describe("bubbleSort", function () {
              it("Makes sort of array by 'Bubble' method. A result should be [135,160,320,456,508,604]", function () {
                 assert.deepEqual(bubbleSort([135,456,320,160,508,604]), [135,160,320,456,508,604]);
              });

              it("Makes sort of array by 'Bubble' method. A result should be [8,16,32,35,46,58]", function () {
                  assert.deepEqual(bubbleSort([35,46,32,16,58,8]), [8,16,32,35,46,58]);
              });

              it("Makes sort of array by 'Bubble' method. A result should be [8,16,20,32,35,46,58,72]", function () {
                  assert.deepEqual(bubbleSort([35,46,32,16,58,8,72,20]), [8,16,20,32,35,46,58,72]);
              });
          });

           describe("selectSort", function () {
                it("Makes sort of array by 'Select' method. A result should be [135,160,320,456,508,604]", function () {
                   assert.deepEqual(selectSort([135,456,320,160,508,604]), [135,160,320,456,508,604]);
                });

                it("Makes sort of array by 'Select' method. A result should be [8,16,32,35,46,58]", function () {
                    assert.deepEqual(selectSort([35,46,32,16,58,8]), [8,16,32,35,46,58]);
                });

                it("Makes sort of array by 'Select' method. A result should be [8,16,20,32,35,46,58,72]", function () {
                    assert.deepEqual(selectSort([35,46,32,16,58,8,72,20]), [8,16,20,32,35,46,58,72]);
                });
            });

            describe("insertSort", function () {
                it("Makes sort of array by 'Insert' method. A result should be [135,160,320,456,508,604]", function () {
                   assert.deepEqual(insertSort([135,456,320,160,508,604]), [135,160,320,456,508,604]);
                });

                it("Makes sort of array by 'Insert' method. A result should be [8,16,32,35,46,58]", function () {
                    assert.deepEqual(insertSort([35,46,32,16,58,8]), [8,16,32,35,46,58]);
                });

                it("Makes sort of array by 'Insert' method. A result should be [8,16,20,32,35,46,58,72]", function () {
                    assert.deepEqual(insertSort([35,46,32,16,58,8,72,20]), [8,16,20,32,35,46,58,72]);
                });
            });