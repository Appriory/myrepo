

       describe("getDayName", function () {
            it("Returns a day-name by number. A result should be 'Monday'", function () {
                assert.equal(getDayName(1), "Monday");
            });

            it("Returns a day-name by number. A result should be 'Tuesday'", function () {
                assert.equal(getDayName(2), "Tuesday");
            });

            it("Returns a day-name by number. A result should be 'Wednesday'", function () {
                assert.equal(getDayName(3), "Wednesday");
            });

            it("Returns a day-name by number. A result should be 'Thursday'", function () {
                assert.equal(getDayName(4), "Thursday");
            });

            it("Returns a day-name by number. A result should be 'Friday'", function () {
                assert.equal(getDayName(5), "Friday");
            });

            it("Returns a day-name by number. A result should be 'Saturday'", function () {
                assert.equal(getDayName(6), "Saturday");
            });

            it("Returns a day-name by number. A result should be 'Sunday'", function () {
                assert.equal(getDayName(7), "Sunday");
            });
        });

        describe("deckardDistBetweenDots", function () {
            it("Seeking distance between dots at decard square. A result should be 3", function () {
                assert.equal(deckardDistBetweenDots(1,2,3,4), 3);
            });

            it("Seeking distance between dots at decard square. A result should be 4", function () {
                assert.equal(deckardDistBetweenDots(2,4,6,8), 4);
            });

            it("Seeking distance between dots at decard square. A result should be 6", function () {
                assert.equal(deckardDistBetweenDots(2,6,10,14), 6);
            });
        });

        describe("makeStringFromThousandNum", function () {
          it("Returns names of numbers from 0 for 999. A result should be 'nine hundred ninety nine'", function () {
              assert.equal(makeStringFromThousandNum(999), "nine hundred ninety nine");
          });

          it("Returns names of numbers from 0 for 999. A result should be 'five hundred five'", function () {
              assert.equal(makeStringFromThousandNum(505), "five hundred five");
          });

          it("Returns names of numbers from 0 for 999. A result should be 'thirty seven'", function () {
              assert.equal(makeStringFromThousandNum(37), "thirty seven");
          });

          it("Returns names of numbers from 0 for 999. A result should be 'one'", function () {
              assert.equal(makeStringFromThousandNum(1), "one");
          });
        });

        describe("getStringNumber", function () {
          it("Returns names of numbers from 0 for 999999999999. A result should be 'nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine'", function () {
              assert.equal(getStringNumber(999999999999), "nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine ");
          });

          it("Returns names of numbers from 0 for 999999999999. A result should be 'five hundred five'", function () {
              assert.equal(getStringNumber(345543), "three hundred forty five thousand five hundred forty three ");
          });

          it("Returns names of numbers from 0 for 999999999999. A result should be 'ten million one hundred one thousand ten '", function () {
              assert.equal(getStringNumber(10101010), "ten million one hundred one thousand ten ");
          });

        });

         describe("makeNumberFromThousandString", function () {
          it("Returns numbers instead words from 0 for 999. A result should be 999", function () {
              assert.equal(makeNumberFromThousandString("nine hundred ninety nine"), 999);
          });

          it("Returns numbers instead words from 0 for 999. A result should be 505", function () {
              assert.equal(makeNumberFromThousandString("five hundred five"), 505);
          });

          it("Returns numbers instead words from 0 for 999. A result should be 37", function () {
              assert.equal(makeNumberFromThousandString("thirty seven"), 37);
          });

        });
