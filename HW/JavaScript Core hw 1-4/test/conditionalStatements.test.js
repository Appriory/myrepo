

describe("oddEvenFunction", function () {
    it("A result should be 6", function () {
        var expected = 6;
        var actual = oddEvenFunction(2, 3);
        assert.equal(actual, expected);
    });
     it("A result should be 5", function () {
        var expected = 5;
        var actual = oddEvenFunction(3, 2);
        assert.equal(actual, expected);
    });
     it("A result should be 43", function () {
        var expected = 43;
        var actual = oddEvenFunction(41, 2);
        assert.equal(actual, expected);
    });
     it("A result should be 82", function () {
        var expected = 82;
        var actual = oddEvenFunction(2, 41);
        assert.equal(actual, expected);
    });
});

describe("numOfQuart", function() {

    it("Determines in which quarter the coordinate point is (4, 4)", function() {
        assert.equal(numOfQuart(4, 4), 'A point is in first quart');
    });

    it("Determines in which quarter the coordinate point is (-4, 4)", function() {
        assert.equal(numOfQuart(-4, 4), 'A point is in second quart');
    });

    it("Determines in which quarter the coordinate point is (-4, -4)", function() {
        assert.equal(numOfQuart(-4, -4), 'A point is in third quart');
    });

    it("Determines in which quarter the coordinate point is (4, -4)", function() {
        assert.equal(numOfQuart(4, -4), 'A point is in four quart');
    });

    it("Determines in which quarter the coordinate point is (0, 0)", function() {
         assert.equal(numOfQuart(0, 0), 'A point is on the intersection of coordinates');
    });

    it("Determines in which quarter the coordinate point is (4, 0)", function() {
        assert.equal(numOfQuart(4, 0), 'A point is between first and four quart on a x axis');
   });

   it("Determines in which quarter the coordinate point is (0, 4)", function() {
        assert.equal(numOfQuart(0, 4), 'A point is between first and second quart on a y axis');
   });
});

describe("sumPosNumbers", function() {

    it("Define a sum of positive numbers = 3, 9, 25", function() {
        assert.equal(sumPosNumbers(3,9,25), 37);
    });

    it("Define a sum of positive numbers = 10, -9, 32", function() {
        assert.equal(sumPosNumbers(10,-9,32), 42);
    });


    it("Define a sum of positive numbers = 6, 8, -25", function() {
        assert.equal(sumPosNumbers(6,8,-25), 14);
    });

 });

  describe("plusOrMultiply", function() {
     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers = 3, 9, 25", function() {
         assert.equal(plusOrMultiply(3,9,25), 678);
     });

     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers = 10, -9, 32", function() {
         assert.equal(plusOrMultiply(10,-9,32), 36);
     });

     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers= -6, 8, -25", function() {
         assert.equal(plusOrMultiply(-6,8,-25), 1203);
     });

     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers= 4, 33, -25", function() {
         assert.equal(plusOrMultiply(4, 33, -25), 15);
     });

     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers= 12, -18, -25", function() {
         assert.equal(plusOrMultiply(12,-18,-25), 5403);
     });

     it("Counts the expression max (a * b * s, a + b + s) +3 of the numbers= -21, 20, 25", function() {
         assert.equal(plusOrMultiply(-21,20,25), 27);
     });
 });

describe("studBallProgram", function() {

    it("A student ball will be = 12", function() {
        assert.strictEqual(studBallProgram(12), "F");
    });

    it("A student ball will be = 30", function() {
        assert.equal(studBallProgram(32), "E");
    });

     it("A student ball will be = 46", function() {
        assert.deepEqual(studBallProgram(52), "D");
    });

    it("A student ball will be = 67", function() {
        assert.equal(studBallProgram(72), "C");
    });

     it("A student ball will be = 79", function() {
        assert.equal(studBallProgram(82), "B");
    });

    it("A student ball will be = 84", function() {
        assert.equal(studBallProgram(92), "A");
    });

});