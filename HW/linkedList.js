var IList = function () {};

IList.prototype.init = () => {}; //+
IList.prototype.toString = () => {}; // +
IList.prototype.getSize = () => {}; // +
IList.prototype.push = (value) => {}; //+
IList.prototype.pop = () => {}; // +
IList.prototype.shift = () => {}; //+
IList.prototype.unshift = (value) => {};//+
IList.prototype.slice = (start, end) => {};
IList.prototype.splice = (start, numberToSplice, ...elements) => {};
IList.prototype.sort = (comparator) => {}; // comparator ==> callback
IList.prototype.get = (index) => {}; // +
IList.prototype.set = (index, element) => {}; // +

var Node = function(value) {
    this.value = value;
    this.next = null;   
};
var LinkedList = function() {
    IList.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(IList.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    var tempNode = this.root;
    var size = 0;
    
    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};
LinkedList.prototype.unshift = function(value) {
    var size = this.getSize();
    var node = new Node(value);
    node.next = this.root;
    this.root = node;

    return size + 1;
};
LinkedList.prototype.init = function(initialArray) {
    for (var i = initialArray.length - 1 ; i >= 0; i--) {
        this.unshift(initialArray[i]);
    }
};

LinkedList.prototype.toString = function () {
    var tempNode = this.root;
    var str = '';
   
    while(tempNode !== null) {
        tempNode.next ? str += tempNode.value + ',': str += tempNode.value + '' ;
        tempNode = tempNode.next    
    }
   
    return str;
}

LinkedList.prototype.push = function(value) {
    var size = this.getSize();
    var tempNode = this.root;
    
    while(tempNode !== null) {

        if(tempNode.next === null){
           tempNode.next = new Node(value);
           return size + 1;
            
        }
        tempNode = tempNode.next; 
    }

}

LinkedList.prototype.pop = function() {
    var size = this.getSize();
    var tempNode = this.root;
    var result;
  
    
    while(size !== 2) {
        size --
        tempNode = tempNode.next; 
    }
    result = tempNode.next.value;
    tempNode.next = null;
    return result;
}

LinkedList.prototype.shift = function() {
    var result = this.root.value;
    this.root = this.root.next;

    // var tempNode = this.root;
    // var result = tempNode.value
    // this.root = tempNode.next;
    return result  
}

LinkedList.prototype.get = function(index) {
    var tempNode = this.root;
    var size = 0;
    
    while(size !== index) {
        tempNode = tempNode.next;
        size++;
    }

    return tempNode.value;
    
}

LinkedList.prototype.set = function(index, element) {
    var tempNode = this.root; 
    var size = 0;
    
    while(size !== index) {
        tempNode = tempNode.next;
        size++;
    }

    tempNode.value = element;

}


var linked = new LinkedList();
linked.init([1,2,3,4,5])

console.log(linked.pop());